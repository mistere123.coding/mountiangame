# Minetest Streetbuilder Mod

This [Minetest](https://www.minetest.net/) mod lets you build streets semi-automatically. You only need to provide some points the street must go through and some street parameters like the width, the block types or whether the street should have piers or not.

The street parameters can be edited and saved within the mod. Streets can be built, removed and re-built with a single command.

![Screenshot example](screenshot1.jpg)

## Limitations and known bugs

The generated streets are very basic. Only standard Minetest nodes are used, no fancy road marking or signs, no traffic lights etc., just cubes. But of course you can edit the streets manually after they have been generated with this mod.

The building algorithm works quite good with plain streets. If the street gets too steep then it may contain artefacts and look broken.

Sometimes the street does not build completely the first time you `execute /street <streetname> build`. Then just rerun the command until the street is complete.

# Usage

The mod provides the Minetest chat command `/street` to edit and build the streets.

    /street

        Show all streets currently saved in the mod storage.

    /street <streetname> set point <N>|next <X>,<Y>,<Z>

        Set point <N> or next point of street <streetname> to position <X>,<Y>,<Z>.
        If no position is given then the current player position is used.

    /street <streetname> set point all <X1>,<Y1>,<Z1> <X2>,<Y2>,<Z2> <Xn>,<Yn>,<Zn>

        Replace all points of street <streetname> with the given positions. Previous points will be deleted!

    /street <streetname> set bordernode|roadnode|linenode|piersnode|pointsnode <nodename>

        Set the border/road surface/center line/bridge piers/original points node type of street <streetname> to <nodename>.
        If <nodename> is omitted the node type will be reset to default.
        If pointsnode is set then the manually set points will be marked (for debugging).

    /street <streetname> set air|pierdist <N>

        Set the number of air nodes above street/the distance between bridge piers.
        If air is not set the default value is used.
        If pierdist is not set no bridge piers are built.

    /street <streetname> ins point <N> <X>,<Y>,<Z>

        Insert point <N> of street <streetname> at position <X>,<Y>,<Z>.
        If no position is given then the current player position is used.

    /street <streetname> del point <N>

        Delete point <N> of street <streetname>.

    /street <streetname> get
    /street <streetname> get all

        Show all parameters of street <streetname>.

    /street <streetname> get point <N>

        Show the position of point <N> in street <streetname>.

    /street <streetname> get point
    /street <streetname> get point all

        Show all points in street <streetname>.

    /street <streetname> get bordernode|roadnode|linenode|piersnode|pointsnode

        Show the node type of the border/road surface/center line/bridge piers/original points of street <streetname>.

    /street <streetname> build

        Generate the street <streetname> in the current world.

    /street <streetname> vanish

        Make the street <streetname> disappear in the current world. The volume around the street will be re-generated to its original state.

    /street <streetname> delete

        Delete all data of street <streetname> from the mod storage.
        If you already generated the street in the world you may want to issue "/street <streetname> vanish" before deleting its data.

## Example

The simplest way to build a street is to fly along the route and mark important points with `/street MyStreet set point next`:


    /street MyStreet set point next
    Point 1 of street "MyStreet" set to player position -131,14,310.
    /street MyStreet set point next
    Point 2 of street "MyStreet" set to player position -96,27,308.
    /street MyStreet set point next
    Point 3 of street "MyStreet" set to player position 19,42,311.
    /street MyStreet set point next
    Point 4 of street "MyStreet" set to player position 76,42,224.
    /street MyStreet set point next
    Point 5 of street "MyStreet" set to player position 156,29,189.
    /street MyStreet set point next
    Point 6 of street "MyStreet" set to player position 254,29,152.

Show points and default values:

    /street MyStreet get all
    Properties of street "MyStreet":
     Point 1: -131,14,310
     Point 2: -96,27,308
     Point 3: 19,42,311
     Point 4: 76,42,224
     Point 5: 156,29,189
     Point 6: 254,29,152
     bordernode is not set. Using default (default:meselamp).
     roadnode is not set. Using default (default:stone).
     linenode is not set. Using default (default:stone).
     piersnode is not set. Using default (default:stone_block).
     pointsnode is not set. Using default (do not mark the original points).
     width is not set. Using default (3).
     air is not set. Using default (5).
     pierdist is not set. Using default (do not build bridge piers).

Modify and show single values:

    /street MyStreet set pierdist 20
    pierdist of street "MyStreet" set to 20.
    /street MyStreet set linenode wool:red
    linenode of street "MyStreet" set to wool:red.

    /street MyStreet get pierdist
    pierdist of street "MyStreet" is set to 20.
    /street MyStreet get linenode
    linenode of street "MyStreet" is set to wool:red.

Build the street:

    /street MyStreet build
    To make the street build reliably we emerge the area around the street ...
    Area emerged.
    Building street 7 blocks broad with 5 nodes of air above...
     Calculating coordinates
     Placing nodes
    Building street done.

![Screenshot MyStreet](screenshot2.jpg)

Make the street disappear again:

    /street MyStreet vanish

## API

You can call the main function `streetbuilder.build_street(plname,points,params,vanish)` from other mods to build a single street.

The function returns a table with the parameters the street was build with.

**Build the street with default parameters:**

    streetbuilder.build_street(plname,{ {x=114,y=7,z=595}, {x=129,y=9,z=588}, {x=137,y=10,z=565}, {x=113,y=9,z=535} })

**Build the street with the given node types:**

    streetbuilder.build_street(plname,{ {x=114,y=7,z=595}, {x=129,y=9,z=588}, {x=137,y=10,z=565}, {x=113,y=9,z=535} },{roadnode='default:stone_block',bordernode='wool:blue'})

**Build bridge piers below the street:**

    streetbuilder.build_street(plname,{ {x=114,y=7,z=595}, {x=129,y=9,z=588}, {x=137,y=10,z=565}, {x=113,y=9,z=535} },{pierdist=30,piersnode='wool:yellow'})

**Mark the original points (for debugging):**

    streetbuilder.build_street(plname,{ {x=114,y=7,z=595}, {x=129,y=9,z=588}, {x=137,y=10,z=565}, {x=113,y=9,z=535} },{pointsnode='wool:red'})

**Delete the street from the world and re-generate the original world around the street:**

    streetbuilder.build_street(plname,{ {x=114,y=7,z=595}, {x=129,y=9,z=588}, {x=137,y=10,z=565}, {x=113,y=9,z=535} },{},true)


# License

[LGPL-3.0-or-later](https://spdx.org/licenses/LGPL-3.0-or-later.html)

