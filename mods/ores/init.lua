

	minetest.register_ore({
		ore_type = "vein",
		ore = "default:stone_with_iron",
		wherein = "default:stone",
		y_max = 31000,
		y_min = -31000,
		noise_params = {
			offset  = 0,
			scale   = 5,
			spread  = {x=400, y=400, z=200},
			seed    = 5390,
			octaves = 4,
			persistence = 0.5,
			lacunarity = 2.0,
			flags = "eased",
		    },
		noise_threshold = 1.85
		    
	})



	minetest.register_ore({
		ore_type = "vein",
		ore = "default:stone_with_copper",
		wherein = "default:stone",
		y_max = 31000,
		y_min = -31000,
		noise_params = {
			offset  = 0,
			scale   = 5,
			spread  = {x=350, y=300, z=150},
			seed    = 5391,
			octaves = 4,
			persistence = 0.5,
			lacunarity = 2.0,
			flags = "eased",
		    },
		noise_threshold = 1.85
		    
	})



	minetest.register_ore({
		ore_type = "vein",
		ore = "default:stone_with_tin",
		wherein = "default:stone",
		y_max = 31000,
		y_min = -31000,
		noise_params = {
			offset  = 0,
			scale   = 5,
			spread  = {x=350, y=300, z=150},
			seed    = 5392,
			octaves = 4,
			persistence = 0.5,
			lacunarity = 2.0,
			flags = "eased",
		    },
		noise_threshold = 1.85
		    
	})




	minetest.register_ore({
		ore_type = "vein",
		ore = "default:stone_with_coal",
		wherein = "default:stone",
		y_max = 31000,
		y_min = -31000,
		noise_params = {
			offset  = 0,
			scale   = 5,
			spread  = {x=250, y=200, z=150},
			seed    = 5388,
			octaves = 4,
			persistence = 0.5,
			lacunarity = 2.0,
			flags = "eased",
		    },
		noise_threshold = 1.75
		    
	})


	minetest.register_ore({
		ore_type = "vein",
		ore = "default:stone_with_gold",
		wherein = "default:stone",
		y_max = 31000,
		y_min = -31000,
		noise_params = {
			offset  = 0,
			scale   = 5,
			spread  = {x=650, y=600, z=300},
			seed    = 5383,
			octaves = 4,
			persistence = 0.5,
			lacunarity = 2.0,
			flags = "eased",
		    },
		noise_threshold = 1.85
		    
	})

	-- mese ore
	minetest.register_ore({
		ore_type = "vein",
		ore = "default:stone_with_mese",
		wherein = "default:stone",
		y_max = 31000,
		y_min = -31000,
		noise_params = {
			offset  = 0,
			scale   = 5,
			spread  = {x=600, y=600, z=200},
			seed    = 5393,
			octaves = 4,
			persistence = 0.5,
			lacunarity = 2.0,
			flags = "eased",
		    },
		noise_threshold = 1.9
		    
	})


	-- diamond ore
	minetest.register_ore({
		ore_type = "vein",
		ore = "default:stone_with_diamond",
		wherein = "default:stone",
		y_max = 31000,
		y_min = -31000,
		noise_params = {
			offset  = 0,
			scale   = 5,
			spread  = {x=900, y=900, z=300},
			seed    = 5394,
			octaves = 4,
			persistence = 0.5,
			lacunarity = 2.0,
			flags = "eased",
			},
		noise_threshold = 1.93
			
	})


	minetest.register_ore({
		ore_type        = "stratum",
		ore             = "default:obsidian",
		wherein         = "default:stone",
		clust_scarcity  = 1,
		y_max           = -2560,
		y_min           = -2960,
		noise_params    = {
			offset = 28,
			scale = 16,
			spread = {x = 128, y = 128, z = 128},
			seed = 90123,
			octaves = 1,
		},
		stratum_thickness = 400,
	})


	minetest.register_ore({
		ore_type        = "stratum",
		ore             = "default:lava_source",
		wherein         = "default:stone",
		clust_scarcity  = 1,
		y_max           = -2660,
		y_min           = -3060,
		noise_params    = {
			offset = 28,
			scale = 16,
			spread = {x = 128, y = 128, z = 128},
			seed = 90122,
			octaves = 1,
		},
		stratum_thickness = 300,
	})




		-- diamond ore for obsidian
	minetest.register_ore({
		ore_type = "vein",
		ore = "default:stone_with_diamond",
		wherein = "default:obsidian",
		y_max = 31000,
		y_min = -31000,
		noise_params = {
			offset  = 0,
			scale   = 5,
			spread  = {x=75, y=75, z=150},
			seed    = 5394,
			octaves = 4,
			persistence = 0.5,
			lacunarity = 2.0,
			flags = "eased",
			},
		noise_threshold = 1.7			
	})


