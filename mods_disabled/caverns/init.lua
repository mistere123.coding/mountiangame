

luamap.register_noise("cavernsmod",{
    type = "3d",
    np_vals = {
        offset = 0,
        scale = 1,
        spread = {x=500, y=500, z=500},
        seed = 59034033,
        octaves = 4,
        persist = 0.63,
        lacunarity = 2.0,
        flags = ""
    },
})

luamap.register_noise("cavernsrooms",{
    type = "3d",
    np_vals = {
        offset = 0,
        scale = 1,
        spread = {x=30, y=30, z=30},
        seed = 34975,
        octaves = 3,
        persist = 0.63,
        lacunarity = 2.0,
        flags = ""
    },
})

luamap.register_noise("cavernsx",{
    type = "3d",
    np_vals = {
        offset = 0,
        scale = 1,
        spread = {x=100, y=10, z=10},
        seed = 5900033,
        octaves = 3,
        persist = 0.63,
        lacunarity = 2.0,
        flags = ""
    },

})

luamap.register_noise("cavernsz",{
    type = "3d",
    np_vals = {
        offset = 0,
        scale = 1,
        spread = {x=10, y=10, z=100},
        seed = 5900033,
        octaves = 3,
        persist = 0.63,
        lacunarity = 2.0,
        flags = ""
    },

})

local air = minetest.registered_nodes["air"]
air.sunlight_propogates = false
minetest.register_node("caverns:air", air)
local c_cave = minetest.get_content_id("caverns:air")
local c_air = minetest.get_content_id("air")



function luamap.logic(noise_vals,x,y,z,seed,original_content)
    return original_content
end

local old_logic = luamap.logic

function luamap.logic(noise_vals,x,y,z,seed,original_content)
    -- get any terrain defined in another mod
    local content = old_logic(noise_vals,x,y,z,seed,original_content)

    if noise_vals.cavernsmod > .7 and (noise_vals.cavernsrooms > .75 or noise_vals.cavernsx > .75 or noise_vals.cavernsz > .75) then
        content = c_cave
    end

    return content
end
